extern crate gl;
extern crate glfw;
extern crate native;

use glfw::Context;

#[start]
fn start(argc: int, argv: *const *const u8) -> int {
    native::start(argc, argv, main)
}

fn main() {
    let glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();

    let (window, _) = glfw
      .create_window(800, 600, "gl_test", glfw::Windowed)
      .expect("Failed to create GLFW window.");

    window.make_current();

    gl::load_with(|s| window.get_proc_address(s));

    while !window.should_close() {
        glfw.poll_events();
        unsafe {
            gl::ClearColor(0.0, 0.5, 0.7, 1.0);
            gl::Clear(gl::COLOR_BUFFER_BIT);
        }
        window.swap_buffers();
    }
}
