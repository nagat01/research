﻿module BezierCurve

open System
open System.Windows          
open System.Windows.Controls 
open System.Windows.Media
open System.Windows.Shapes


type Lever(p0:Po sh, p1:Po sh, ca:Ca) =
  let ellipse = Ellipse(Fill=Brushes.Red) $ wh 10 10 $ ca.addTo p1.v
  let line    = Line(Stroke=Brushes.DarkBlue) $ p0p1 p0.v p1.v $ ca
  do 
    ellipse.RenderTransform <- TranslateTransform(-5. , -5.)

    p0 => line.set_p1 
    p1 => fun p1 -> line.p2 <- p1; p1.mv ellipse
  member __.Circle = ellipse


type BezierFigure(p0:Po sh, p1:Po sh, p2:Po sh, p3:Po sh) =
  let bezierSegment = BezierSegment(p1.v, p2.v, p3.v, true)
  let figure = PathFigure(p0.v, [bezierSegment], false)
  do
    p0 => figure.set_StartPoint
    p1 => bezierSegment.set_Point1
    p2 => bezierSegment.set_Point2
    p3 => bezierSegment.set_Point3
  member __.Figure = figure


let mutable currentPoint : Po sh opt = None


let ca = Ca() $ "eef".bg

let p0 = sh (po 50  100)
let p1 = sh (po 100 180)
let p2 = sh (po 150 20)
let p3 = sh (po 200 100)

let startLever = Lever(p0,p1,ca)
let endLever   = Lever(p3,p2,ca)



let bezierFigure = BezierFigure (p0, p1, p2, p3)
let path = Path(Stroke=Brushes.Red, StrokeThickness=2.) $ ca
let geom = PathGeometry([bezierFigure.Figure])
path.Data <- geom

startLever.Circle.mdown =+ { currentPoint <- Some p1 }

endLever.Circle.mdown =+ { currentPoint <- Some p2 }

ca.lDrag => fun e ->
  currentPoint |%| fun p -> p <*- p.v + e.Vec

