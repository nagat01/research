﻿module BezierCurveAppender

open System open System.Windows open System.Windows.Controls  

open System.Windows.Input
open System.Windows.Media
open System.Windows.Shapes


let ca = Ca() $ "eef".bg
           
let mkControlPoint (ca:Ca) (co:Co) (p:Po sh) =
  let cp = Ellipse () $ fill co $ wh 12 12 $ zIdx 1 $ 0.3.opa $ ca.addTo p.v
  cp.RenderTransform <- TranslateTransform (-6., -6.)
  p => fun p -> p.mv cp
  cp.lDragAbs => fun e -> p <*- e.Cur
  cp.RaiseEvent(MouseButtonEventArgs(Mouse.PrimaryDevice, Environment.TickCount, MouseButton.Left, RoutedEvent = UIElement.PreviewMouseDownEvent))
  cp

[<I>]type IVisibleSettable = abstract isVis: b->u

type EditablePoint(p0:Po sh, ca:Ca) = 
  let cp = mkControlPoint ca "0f0".co p0 
  interface IVisibleSettable with 
    member __.isVis isVis = cp.Vis isVis

type EditableLever(p0:Po sh, p1:Po sh, ca:Ca) =
  let cp = mkControlPoint ca "f00".co p1
  let line = Line(Stroke=Brushes.Red) $ p0p1 p0.v p1.v $ 0.3.opa $ ca
  do 
    p0 => line.set_p1 
    p1 => line.set_p2 
  interface IVisibleSettable with 
    member __.isVis isVis = isVis $ cp.Vis |> line.Vis

type BezierCurveAppender (ca:Ca, newBezier : kea obs) =
  let mutable phase = 0
  let mutable curPathFigure = PathFigure()
  let pathGeometry = PathGeometry [curPathFigure]
  let path = Path(Data=pathGeometry, Stroke=Brushes.Black)
  let mutable setPointHandler = Def
  let mutable lastP3 = sh Po.None
  
  let mkBezierCurve (p0:Po sh) p =
    let p1 = sh Po.None
    let p2 = sh Po.None
    let p3 = sh Po.None
    lastP3 <- p3
    
    let setPoint p =   
      [|p3; p1; p2|].[phase-1 .. 2] |%| fun __ -> __ <*- p
      match phase with
      | 1 ->
        EditablePoint(p3, ca).ig
      | 2 -> 
        EditableLever(p0, p1, ca).ig
      | 3 -> 
        EditableLever(p3, p2, ca).ig
      | _ -> ()

    setPointHandler <- setPoint

    let bezierSegment = BezierSegment() $ curPathFigure
    p1 => bezierSegment.set_Point1 
    p2 => bezierSegment.set_Point2 
    p3 => bezierSegment.set_Point3 

    setPoint p
  do ca.lmdown.[fun e -> e.Source = box ca] => fun e ->
    let p = e.GetPosition ca
    if phase = 0 then
      lastP3 <- sh Po.None
      lastP3 => curPathFigure.set_StartPoint
      lastP3 <*- p
      EditablePoint(lastP3, ca).ig

    elif phase = 1 then
      mkBezierCurve lastP3 p
    elif phase <= 3 then
      setPointHandler p
    phase <- phase % 3 + 1
  do
    newBezier =+ {
      curPathFigure <- PathFigure ()
      pathGeometry.Figures.Add curPathFigure
      phase <- 0 }
                                   
  member __.Path = path

let bezierCurveAppender = BezierCurveAppender(ca, S.wnd.kdown Key.A)

bezierCurveAppender.Path |> ca