﻿module TurtleGraphics
//Author: Scott Brown //10/10/2010 //This code is licensed under the Code Project Open License (CPOL)

open System open System.Windows open System.Windows.Controls
open System.Windows.Markup
open System.Windows.Media
open System.Windows.Shapes
open System.Collections.Generic
open System.Xml
       
//Make a Canvas subclass which supports ScrollViewer
type ScrollableCanvas() = 
  inherit Ca()
  override __.MeasureOverride limit: Sz =
    let availableSize = sz Double.PositiveInfinity Double.PositiveInfinity
    let mutable maxSize = sz 0 0
    for element in __.cs do
      element.Measure(availableSize).ig
      let pnt = element.po + element.DesiredSize.vec
      if Double.IsNaN pnt.X &&~ Double.IsNaN pnt.Y then
        maxSize <- sz (max maxSize.w pnt.X) (max maxSize.h pnt.Y)
    maxSize

let wnd = Window ()
wnd.Show()
let dp = Dpnl () $ wnd
let  sp = HSpnl () $ dp.top
let   cmbb描画方法 = Cmb(SelectedIndex=0) $ sp
let   cmbb回数 = Cmb() $ itemsi 0 [1..20] $ sp
let   btn描画 = Btn "描画" $ sp
let  scrollViewer = ScrollViewer() $ hscrAuto $ vscrAuto $ dp.bottom
let   ca = ScrollableCanvas() $ scrollViewer
let    arrow = Polygon(Fill=Brushes.Red, RenderTransformOrigin=po 0.5 0.5, Points=PointCollection[po -12.5 10.; po 12.5 0.; po -12.5 -10.]) $ ca

//
// model
//
type Turtle = { mutable P:Po; mutable Angle:f }
                                  
let degreeToRadian deg = deg * (Math.PI / 180.0)

let turtle = {P = po (wnd.ActualWidth/2.) (wnd.ActualHeight/2.); Angle = 0.}

let rotate deg = 
  turtle.Angle <- turtle.Angle + deg
  arrow.RenderTransform <- RotateTransform turtle.Angle           
        
let moveTo po = 
  turtle.P <- po
  po.mv arrow
                 
let forward isDraw distance = 
  let angle = degreeToRadian turtle.Angle
  let next = turtle.P + vec (distance * cos angle) (distance * sin angle)
  if isDraw then Line() $ stroke 1 Co.rand $ p0p1 turtle.P next |> ca
  moveTo next

let clear() = 
  ca.clear
  ca.add arrow
  moveTo <| wnd.asz.map(_1 / 2.0).po
  turtle.Angle <- 0.
  arrow.RenderTransform <- RotateTransform turtle.Angle

//
// L System Code  
//
let mutable length = 5.0
let turtleStates = Stack<Turtle>()

type LSystem = {Start:s;  Rules:IDictionary<char,s>;  Angle:f}       
  
let generateString (lsys:LSystem) n =
  let rec gen (acc:s) n = if n = 0 then acc else gen (acc.collect lsys.Rules.get_Item) (n - 1) 
  gen lsys.Start n
        
let pushTurtle() = 
  turtleStates.Push {P=turtle.P; Angle=turtle.Angle}
        
let popTurtle() = 
  let poppedTurtle = turtleStates.Pop()
  turtle.P <- poppedTurtle.P
  turtle.Angle <- poppedTurtle.Angle
           
let drawCommand (lsys:LSystem) = function
  | 'F' -> forward true length 
  | 'G' -> forward true length
  | 'f' -> forward false length
  | '+' -> rotate lsys.Angle
  | '-' -> rotate -lsys.Angle
  | '[' -> pushTurtle()
  | ']' -> popTurtle()
  | _   -> ()
  
let drawLSystem lsys n = 
  generateString lsys n |%| drawCommand lsys
     
//
// Examples
//
let rec polyspi angle inc side times =
  if times > 0 then
    forward true side
    rotate angle
    polyspi angle inc (side + inc) (times - 1)

//Some curried polyspirals    
let polyspi90 = polyspi 90. 5. length

let polyspi95 = polyspi 95. 1. length

let polyspi117 = polyspi 117. 3. length

//Some L-systems     
let kochCurve =
  let rules = 
   ['F',"F+F-F-F+F"
    '+',"+"
    '-',"-" ]
  drawLSystem {Start="F"; Rules=dict rules; Angle=90.}

let dragonCurve =
  let rules =
   ['F',"F"
    '+',"+"
    '-',"-"
    'X',"X+YF"
    'Y',"FX-Y" ]
  drawLSystem {Start="FX"; Rules=dict rules; Angle=90.}
  
let branching =
  let rules =
   ['F',"FF"
    'X',"F-[[X]+X]+F[+FX]-X"
    '+',"+"
    '-',"-"
    '[',"["
    ']',"]" ]
  drawLSystem {Start = "X"; Rules=dict rules; Angle=22.5}
  
let sierpinskiTriangle =
  let rules = 
   ['F',"G-F-G"
    'G',"F+G+F"
    '+',"+"
    '-',"-" ]
  drawLSystem {Start="F"; Rules=dict rules; Angle=60.} 
  
//http://tripatlas.com/Penrose_tiling
let penroseRhombus = 
  let rules =
   ['F',""
    '+',"+"
    '-',"-"
    '[',"["
    ']',"]"
    '6',"8F++9F----7F[-8F----6F]++"
    '7',"+8F--9F[---6F--7F]+"
    '8',"-6F++7F[+++8F++9F]-"
    '9',"--8F++++6F[+9F++++7F]--7F" ]
  drawLSystem {Start="[7]++[7]++[7]++[7]++[7]"; Rules=dict rules; Angle=36.}

[ polyspi90
  polyspi95
  polyspi117
  kochCurve
  dragonCurve
  branching
  sierpinskiTriangle
  penroseRhombus
].iter cmbb描画方法.add 


btn描画 =+ {
  let draw = cmbb描画方法.item :?> (i->u)
  let n = cmbb回数.item :?> i
  draw n }