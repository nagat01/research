KagPackageView = require './kag-package-view'
{CompositeDisposable} = require 'atom'

module.exports = KagPackage =
  kagPackageView: null
  modalPanel: null
  subscriptions: null

  activate: (state) ->
    @kagPackageView = new KagPackageView(state.kagPackageViewState)
    @modalPanel = atom.workspace.addModalPanel(item: @kagPackageView.getElement(), visible: false)

    # Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    @subscriptions = new CompositeDisposable

    # Register command that toggles this view
    @subscriptions.add atom.commands.add 'atom-workspace', 'kag-package:toggle': => @toggle()

  deactivate: ->
    @modalPanel.destroy()
    @subscriptions.dispose()
    @kagPackageView.destroy()

  serialize: ->
    kagPackageViewState: @kagPackageView.serialize()

  toggle: ->
    console.log 'KagPackage was toggled!'

    if @modalPanel.isVisible()
      @modalPanel.hide()
    else
      @modalPanel.show()
