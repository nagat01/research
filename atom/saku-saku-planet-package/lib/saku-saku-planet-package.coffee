SakuSakuPlanetPackageView = require './saku-saku-planet-package-view'
{CompositeDisposable} = require 'atom'
child_process = require 'child_process'


module.exports = SakuSakuPlanetPackage =
  sakuSakuPlanetPackageView: null
  modalPanel: null
  subscriptions: null

  activate: (state) ->
    @subscriptions = new CompositeDisposable
    @subscriptions.add atom.commands.add 'atom-workspace', 'saku-saku-planet:toggle': => @toggle()
    @subscriptions.add atom.commands.add 'atom-workspace', 'saku-saku-planet:run': => @run()

  deactivate: ->
    @modalPanel.destroy()
    @subscriptions.dispose()
    @sakuSakuPlanetPackageView.destroy()

  serialize: ->
    sakuSakuPlanetPackageViewState: @sakuSakuPlanetPackageView.serialize()

  toggle: ->
    console.log 'SakuSakuPlanetPackage was toggled!'

  run: ->
    child_process.exec 'C:\\P\\Sites\\svg\\build.sh'
