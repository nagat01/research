# math
translator = (xyzs) ->
  m = (a, b) -> a + Math.random() * (b - a)
  rx = m 0.1, 2
  ry = m 1,   2
  rz = m 1,   2
  qx = m 0, 1
  qy = m 0, 1
  qz = m 0, 1
  qw = m 0, 1
  qAll = Math.sqrt(qx * qx + qy * qy + qz * qz + qw * qw)
  qx /= qAll
  qy /= qAll
  qz /= qAll
  qw /= qAll
  xx = qx * qx  
  xy = qx * qy
  xz = qx * qz
  xw = qx * qw
  yy = qy * qy
  yz = qy * qz
  yw = qy * qw
  zz = qz * qz
  zw = qz * qw
  for [x, y, z] in xyzs
    x = x * rx
    y = y * ry
    z = z * rz
    x2 = (1-2*(yy+zz))*x + 2*(xy-zw)*y + 2*(xz+yw)*z
    y2 = 2*(xy+zw)*x + (1-2*(zz+xx))*y + 2*(yz-xw)*z
    z2 = 2*(xy-yw)*x + 2*(yz+xw)*y + (1-2*(xx+yy))*z
    x3 = x2*100 + 200 
    y3 = y2*100 + 300
    [x3, y3]


# cube
psCube = []
for x in [-1, 1]
  for y in [-1, 1]
    for z in [-1, 1]
      psCube.push [x,y,z]

lsCube = []
one = (a, b) -> if a is b then 1 else 0
for [x0,y0,z0], i in psCube
  for [x1,y1,z1], j in psCube
    if one(x0,x1) + one(y0,y1) + one(z0,z1) is 2
      lsCube.push [i, j] 


isCorrecting = false


# funcs
nextCube = ->
  tooNear = ([x0,y0], [x1,y1]) ->
    x = x0 - x1
    y = y0 - y1
    x*x + y*y < 10000
  xys
  next = true
  while next
    xys = translator psCube
    next = false
    for [x, y] in xys
      if not(0 < x < 400 and 0 < y < 600)
        next = true
    for [i, j] in lsCube
      if tooNear xys[i], xys[j]
        next = true
  for [i, j] in lsCube
    [xys[i], xys[j]]

newCanvas = (id) ->
  canvas = document.getElementById id
  canvas.getPoint = (e) -> 
    rect = canvas.getBoundingClientRect()
    [e.pageX - rect.left, e.pageY - rect.top]
  canvas

newDrawingContext = (canvas) ->
  dc = canvas.getContext "2d"
  dc.clear = ->
    dc.fillStyle = "#fff"
    dc.fillRect 0, 0, canvas.width, canvas.height
    h = (y, co) -> dc.drawLine [[0,y], [400,y]], co
    v = (x, co) -> dc.drawLine [[x,0], [x,600]], co
    h 150, "fed"
    h 300, "7f7"
    h 450, "fed"
    v 100, "fed"
    v 200, "7f7"
    v 300, "fed"

  dc.drawLine = ([[x0,y0], [x1,y1]], co="000") ->
    dc.strokeStyle = "#" + co
    dc.beginPath()
    dc.moveTo(x0, y0)
    dc.lineTo(x1, y1)
    dc.stroke()

  dc.drawLines = (ls, co) ->
    dc.strokeStyle = "#" + co
    for [[x0,y0], [x1,y1]] in ls 
      dc.beginPath()
      dc.moveTo(x0, y0)
      dc.lineTo(x1, y1)
      dc.stroke()

  dc.drawPolygon = (ps, co) ->
    dc.strokeStyle = "#" + co
    dc.beginPath()
    dc.moveTo ps[0].x, ps[0].y
    for [x,y] in ps
      dc.lineTo x, y
    dc.closePath()
    dc.stroke()
  dc.clear()
  dc


# ui
image  = newCanvas "cnvImage"
canvas = newCanvas "cnvCanvas"
button = document.getElementById "btnCurrent"

# state
prev = null
dcImage  = newDrawingContext image
dcCanvas = newDrawingContext canvas


# funcs
cube = null
next = ->
  dcCanvas.clear()
  dcImage.clear()
  cube = nextCube()
  dcImage.drawLines cube, "000"
  button.innerHTML = "採点する"

correct = ->
  dcCanvas.drawLines cube, "f77"
  button.innerHTML = "次の問題へ"


# events
canvas.onmousedown  = (e) -> prev = canvas.getPoint e
canvas.onmouseup    = (e) -> prev = null
canvas.onmouseleave = (e) -> prev = null

canvas.onmousemove = (e) ->
  cur = canvas.getPoint e
  if prev?
    dcCanvas.drawLine [prev, cur]
    prev = cur

button.onclick = -> 
  if isCorrecting
    next()
  else
    correct()
  isCorrecting = !isCorrecting



# init
next()


# not in use
# dc.font = "20px Georgia"
# dc.fillText "#{p0.x}, #{p0.y}, #{p1.x}, #{p1.y}", 20, 20
# alert "#{p0.x}, #{p0.y}, #{p1.x}, #{p1.y}"