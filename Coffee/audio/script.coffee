
epsilon = Math.pow(0.1, 10)

freq = (octave, chromatic) ->
  440 * Math.pow(2, (chromatic + (octave * 12) - 57) / 12.0)

C  = (octave) -> freq octave, 0
CS = (octave) -> freq octave, 1
D  = (octave) -> freq octave, 2
DS = (octave) -> freq octave, 3
E  = (octave) -> freq octave, 4
F  = (octave) -> freq octave, 5
FS = (octave) -> freq octave, 6
G  = (octave) -> freq octave, 7
GS = (octave) -> freq octave, 8
A  = (octave) -> freq octave, 9
AS = (octave) -> freq octave, 10
B  = (octave) -> freq octave, 11

AudioNode::c = (an) ->
  @connect an
  an

class Effect
  ctx = new AudioContext
  frameRate = 44100.0

  grade: ->
    tStart = ctx.currentTime
    set = (dur, type, notes) ->
      tEnd = tStart + dur
      osc = do ctx.createOscillator
      osc.type = type
      offset = 0
      for [note, dur] in notes
        osc.frequency.setValueAtTime note, tStart + offset
        offset += dur * 0.05
      gain = do ctx.createGain
      gain.gain.setValueAtTime 0.1, tStart
      gain.gain.exponentialRampToValueAtTime 0.01, tEnd
      osc.c(gain).c(ctx.destination)
      osc.start tStart
      osc.stop tEnd

    set 1.0, "sine", [[F(5), 3], [B(5), 2]]
    set 1.0, "sine", [[E(4), 2], [G(4), 2]]
    set 1.0, "sine", [[D(4), 1], [E(4), 2]]


  hover: ->
    osc = do ctx.createOscillator
    gain = do ctx.createGain

    osc.c(gain).c(ctx.destination)

    tStart = ctx.currentTime
    tEnd = tStart + 0.1
    osc.frequency.value = C 4
    gain.gain.setValueAtTime 0.5, tStart
    gain.gain.exponentialRampToValueAtTime epsilon, tEnd
    osc.start tStart
    osc.stop tEnd

effect = new Effect

do effect.grade
