open FStap open System open System.Windows open System.Windows.Controls open System.Reflection
[<product "TKTest"; version "0.0"; company "ながと"; copyright "Copyright © ながと 2014";
  title "TKTest: __"; STAThread>] SET_ENG_CULTURE

open System.Windows.Forms
open System.Windows.Forms.Integration
open System.Drawing
open System.Drawing.Imaging
open System.Runtime.InteropServices

let wnd  = Window' "TKTest"
let host = new WindowsFormsHost() $ wnd $ "000".bg
let pbx  = new PictureBox() $ host.set_Child

let img  = new Bitmap(600, 600) $ pbx.set_Image

wnd.Loaded =+ {
  let rect = Rectangle(250, 250, 100, 100)
  let data = img.LockBits(rect, ImageLockMode.WriteOnly, PixelFormat.Format32bppPArgb)
  let array = [| for i in 0 .. 10000 - 1 -> 0xff00ffff |]
  Marshal.Copy(array, 0, data.Scan0, 10000)
  img.UnlockBits(data)
  }

wnd.run
