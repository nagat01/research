﻿[<AutoOpen>]
module Wpf

open System
open System.Windows
open System.Windows.Controls
open System.Windows.Controls.Primitives
open System.Windows.Media
open System.Windows.Media.Effects


// extensions
type Panel with
  member __.add uie = __.Children.Add uie |> ignore

type DockPanel with
  member __.addTo dock uie = 
    DockPanel.SetDock(uie, dock)
    __.add uie

type Grid with
  member __.addColumn value ty =
    __.ColumnDefinitions <+ ColumnDefinition(Width = GridLength(value, ty))
  member __.addTo c r value ty uie =
    Grid.SetColumn(uie, c)
    Grid.SetRow(uie, r)
    __.addColumn value ty
    __.add uie


// wpf
let margin  width (fe:FrameworkElement) = fe.Margin       <- Thickness width
let padding width (control:Control)     = control.Padding <- Thickness width

let width  width  (fe:FrameworkElement) = fe.Width  <- width
let height height (fe:FrameworkElement) = fe.Height <- height

let foreground code (__:Control) = __.Foreground <- mkBrush code

let bold size (control:Control) = 
  control.FontSize <- size
  control.FontWeight <- FontWeights.Bold

let hca ha (control:Control) = 
  control.HorizontalContentAlignment <- ha
