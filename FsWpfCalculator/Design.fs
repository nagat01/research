﻿[<AutoOpen>]
module Design

open System
open System.Windows
open System.Windows.Controls
open System.Windows.Controls.Primitives
open System.Windows.Media
open System.Windows.Media.Effects


// design funcs
let mkTrigger prop value setters = 
  let trigger = Trigger(Property = prop, Value = value)
  for prop, value in setters do
    trigger.Setters <+ Setter(prop, value, "border")
  trigger

let mkColor code = ColorConverter.ConvertFromString ("#" + code) :?> Color
let mkBrush code = SolidColorBrush(mkColor code)


//   liner gradient brush
let mkGradientStop code offset =
  GradientStop(mkColor code, offset)

let mkGradientStops props = 
  GradientStopCollection
    [ for code, offset in props -> mkGradientStop code offset ]

let mkLinerGradientBrush gradientStops =
  LinearGradientBrush(mkGradientStops gradientStops, Point(0.5, 0.), Point(0.5, 1.))


//   effect
let mkDropShadowEffect color =
  DropShadowEffect(ShadowDepth = 0., BlurRadius = 8., Color = mkColor color)


//   trigger
let mkTriggerButton prop value (co0, co1, co2, co3) =
  mkTrigger prop value [
    Control.BackgroundProperty, mkLinerGradientBrush [ co0, 0.; co1, 0.3; co2, 1.] :> obj
    UIElement.EffectProperty, mkDropShadowEffect co3 :> obj 
    ]

// control template
let mkFactory (ty:Type) name values = 
  let factory = FrameworkElementFactory(ty, Name = name)
  for prop, value in values do
    factory.SetValue(prop, value)
  factory

let mkControlTemplate ty factory triggers =
  let template = ControlTemplate(ty, VisualTree = factory)
  for trigger in triggers do
    template.Triggers <+ trigger
  template


//   buttun template
let mkFactoryBorder (co0, co1, co2, co3) = 
  let factoryContentPresenter =
    mkFactory typeof<ContentPresenter> "contentPresenter" [
      FrameworkElement.VerticalAlignmentProperty,   VerticalAlignment.Center :> obj
      FrameworkElement.HorizontalAlignmentProperty, HorizontalAlignment.Center :> obj
    ]
  let factoryBorder = 
    mkFactory typeof<Border> "border" [
      Border.CornerRadiusProperty,    CornerRadius 10. :> obj
      Border.BorderThicknessProperty, Thickness 1. :> obj
      Border.BorderBrushProperty,     Brushes.Gray :> obj
      Control.BackgroundProperty,     mkLinerGradientBrush [ co0, 0.; co1, 0.4; co2, 1. ] :> obj
      UIElement.EffectProperty,       mkDropShadowEffect co3 :> obj
    ]
  factoryBorder.AppendChild factoryContentPresenter
  factoryBorder


let mkTemplateButton colors colorsPressed = 
  mkControlTemplate typeof<Button> (mkFactoryBorder colors) [
    mkTriggerButton UIElement.IsMouseOverProperty true  ("fea", "fb0", "fd9", "de0")
    mkTriggerButton ButtonBase.IsPressedProperty  true  colorsPressed
    mkTriggerButton Button.IsEnabledProperty      false ("ddd", "ddd", "ddd", "ddd")
  ]


let green = mkTemplateButton ("3f3", "1a0", "8f8", "0af") ("1d0", "2a1", "040", "060") 
let blue  = mkTemplateButton ("0df", "1bf", "8ff", "0af") ("56f", "01f", "76f", "10a") 
let red   = mkTemplateButton ("f99", "f88", "d00", "611") ("d11", "400", "a11", "060") 
