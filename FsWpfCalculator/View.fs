﻿[<AutoOpen>]
module View

open System
open System.Windows
open System.Windows.Controls
open System.Windows.Controls.Primitives
open System.Windows.Media
open System.Windows.Media.Effects

let mkButton content color =
  Button(Content = content, Template = color)
  |*>^ bold 24. |*>^ foreground "fff"

let mkTextBox fontSize =
  TextBox(
    Background = mkLinerGradientBrush [ "eee", 0.; "ddd", 0.4; "fff", 1.],
    IsReadOnly = true
    )
  |*>^ bold fontSize |*>^ margin 3.

// ui

//   state label
let state =
  Label(
    Foreground = mkBrush "cfc",
    Background = mkLinerGradientBrush ["766", 0.; "433", 0.25; "777", 1.] 
    )
  |*>^ bold 30. |*>^ margin 3. |*>^ padding 0. |*>^ hca HA.Center
state.sized.Add <| fun _ -> state.Width <- state.RenderSize.Height

//   buttons
let buttons = UniformGrid(Columns=8, Rows=6) 
let addButton color content = 
  mkButton content color |*>^ buttons.add 
  |*>^ margin 3. |*>^ padding 6.
let green = addButton green
let blue  = addButton blue
let red   = addButton red

// compose gui
let window       = Window(Title = "FsWpfCalculator")
let  dpMain      = DockPanel()   |*>^ window.set_Content
let   expression = mkTextBox 10. |*>^ dpMain.addTo Dock.Top |*>^ hca HA.Right
let   dpNums     = Grid()        |*>^ dpMain.addTo Dock.Top 
let    prevNum   = mkTextBox 30. |*>^ dpNums.addTo 0 0 0.25 GridUnitType.Star |*>^ foreground "cfc7" |*>^ hca HA.Right
let    _         = state         |*>^ dpNums.addTo 1 0 0.   GridUnitType.Auto
let    curNum    = mkTextBox 30. |*>^ dpNums.addTo 2 0 0.75 GridUnitType.Star |*>^ hca HA.Right
let   _          = buttons       |*>^ dpMain.addTo Dock.Top |*>^ height 240.
let   history    = mkTextBox 10. |*>^ dpMain.addTo Dock.Bottom |*>^ hca HA.Left


let mc     = green "MC" 
let mr     = green "MR" 
let ms     = green "MS" 
let mplus  = green "M+" 
let mminus = green "M-" 
let bs     = blue "BS" 
let ca     = blue "CA"  
let _      = Label() |*>^ buttons.add

let ce        = blue "CE" 
let c         = blue "C"  
let plusMinus = blue "±"  
let equal     = blue "=" 
let reciploc  = blue "1/x" 
let mod'      = red "mod"
let log'      = red "log"
let pi        = red "π"

let d7        = blue "7" 
let d8        = blue "8"
let d9        = blue "9"
let div       = blue "÷"
let root      = blue "√"
let sin'      = red "sin"
let sinh'     = red "sinh"
let factorial = red "n!"

let d4      = blue "4"
let d5      = blue "5"
let d6      = blue "6"
let mul     = blue "×"
let percent = blue "%"
let cos'    = red "cos"
let cosh'   = red "cosh"
let frac    = red "frac"

let d1       = blue "1"
let d2       = blue "2"
let d3       = blue "3"
let sub      = blue "-"
let subEqual = blue "-="
let tan'     = red "tan"
let tanh'    = red "tanh"
let int'     = red "int"

let d0       = blue "0"
let d00      = blue "00"
let dot      = blue "."
let add      = blue "+"
let addEqual = blue "+="
let pow'     = red "xy"
let exp'     = red "exp"

let unaryOps = [
  frac; int'; factorial;
  reciploc; root; exp'; log';
  sin'; sinh'; cos'; cosh'; tan'; tanh' ]
let binOps = [add; sub; mul; div; mod'; pow']