﻿open System
open System.Windows
open System.Windows.Controls
open System.Windows.Controls.Primitives

// model
let mutable adderSum = 0.
let mutable memory = 0.
let mutable lastToken = Token.None

// funcs
let appendExpression text =
    expression.AppendText <| " " + text 

let AppendHistory<'__> =
  history.AppendText <| expression.Text + "\r\n"
  expression.Text <- ""

let BinOp<'__>   = BinOp.fromString (string state.Content)
let PrevNum<'__> = parseDouble prevNum.Text
let CurNum<'__>  = parseDouble curNum.Text

let BinOpResult<'__> =
  match PrevNum, BinOp, CurNum with 
  | Some a, Some op, Some b -> op.exec a b |> Some 
  | _ -> None

let AddNum<'__>  =
  match lastToken with 
  | Token.Adder -> defaultArg PrevNum 0.
  | _ -> defaultArg BinOpResult (defaultArg CurNum 0.)


let CalcPercent<'__> =
  match PrevNum, BinOp, CurNum with
  | Some a, Some (Add|Sub|Mul|Div as op), Some b ->
    match op with 
    | Add -> a * (1. + b / 100.) |> Some
    | Sub -> a * (1. - b / 100.) |> Some
    | Mul -> a * (b / 100.)      |> Some
    | Div -> a / (1. + b / 100.) |> Some
    | _ -> None
  | _, _, Some b when b <> 0. -> b / 100. |> Some
  | _ -> None
  
let Validate<'__> =
  if curNum.Text = "" then curNum.Text <- "0"
  let curNum = curNum.Text
  let isBinOpSafe = 
    match BinOpResult with 
    | Some result -> numOk result
    | _ -> true
  let isZero = CurNum = Some 0. 
  let dotted = curNum.Contains "."

  let curNumOk = CurNum.IsSome && CurNum <> Some 0.
  mc.IsEnabled     <- memory <> 0. 
  mr.IsEnabled     <- memory <> 0. 
  ms.IsEnabled     <- curNumOk
  mplus.IsEnabled  <- curNumOk
  mminus.IsEnabled <- curNumOk

  equal.IsEnabled <- BinOpResult.IsSome && lastToken = Token.Num && isBinOpSafe
  d0.IsEnabled  <- not isZero || dotted
  d00.IsEnabled <- not isZero || dotted
  dot.IsEnabled <- not dotted

  for unaryOp in unaryOps do
    unaryOp.IsEnabled <-
      match UnaryOp.fromString(string unaryOp.Content), CurNum with
      | Some op, Some n -> op.exec n |> numOk
      | _ -> false

  for binOp in binOps do 
    binOp.IsEnabled <- lastToken <> Token.None && isBinOpSafe
  percent.IsEnabled <- CalcPercent.IsSome
  addEqual.IsEnabled <- AddNum <> 0.
  subEqual.IsEnabled <- AddNum <> 0.

let CalcBinOp<'__> =
  match BinOpResult with
  | Some result ->
    appendExpression (string BinOp.Value)
    appendExpression curNum.Text
    prevNum.Text <- ""
    curNum.Text  <- string result
    state.Content <- "="
    Validate
    Some result
  | _ -> None

let ClearEntry<'__> =
  curNum.Text <- ""
  lastToken <- if BinOp.IsSome then Token.BinOp else Token.None
  Validate

let Clear<'__> =
  curNum.Text     <- ""
  state.Content   <- ""
  prevNum.Text    <- ""
  expression.Text <- ""
  lastToken       <- Token.None
  adderSum <- 0.
  Validate

let Negate<'__> =
  try 
    let text = curNum.Text
    let text = if text.[0] = '-' then text.[1..] else "-" + text
    Double.Parse text |> ignore
    curNum.Text <- text
    Validate
  with _ -> ()

let ExecEqual<'__> =
  if CalcBinOp.IsSome then
    appendExpression "="
  appendExpression curNum.Text
  AppendHistory
  Validate

// event funcs
let updateMemory value =
  memory <- value
  lastToken <- Token.EqualOp
  Validate

let input text button =
  button =+ fun _ -> 
    let prev = 
      match lastToken with  
      | Token.BinOp | Token.EqualOp | Token.Adder -> "" 
      | _ -> if curNum.Text = "0" then "" else curNum.Text
    let text = prev + text
    let text = if text = "." then "0." else text
    if (parseDouble text).IsSome then
      curNum.Text <- text
      lastToken <- Token.Num 
    Validate

let unaryOp (unaryOp:UnaryOp) button =
  button =+ fun _ ->
    if CurNum.IsSome then
      curNum.Text <- unaryOp.exec CurNum.Value |> string
      lastToken <- Token.Num
      Validate

let binOp binOp button =
  button =+ fun _ ->
    match lastToken with 
    | Token.Num | Token.EqualOp | Token.Adder ->
      if CalcBinOp.IsNone then
        appendExpression curNum.Text
      prevNum.Text <- curNum.Text
    | _ -> ()
    state.Content <- string binOp
    lastToken <- Token.BinOp
    Validate


let adder op =
  prevNum.Text <- string AddNum
  adderSum <- op adderSum AddNum
  curNum.Text <- string adderSum
  lastToken <- Token.Adder
  Validate
  
// events
mc =+ fun _ -> 
  memory <- 0.
  Validate
mr =+ fun _ -> 
  curNum.Text <- string memory
  Validate
ms =+ fun _ -> 
  updateMemory CurNum.Value
mplus  =+ fun _ -> 
  updateMemory <| memory + CurNum.Value
mminus =+ fun _ -> 
  updateMemory <| memory - CurNum.Value

bs =+ fun _ -> 
  let num = curNum.Text
  if num.Length >= 1 then
    curNum.Text <- num.Substring(0, num.Length - 1)
  Validate

ce =+ fun _ -> ClearEntry
c  =+ fun _ -> Clear
ca =+ fun _ -> Clear; history.Text <- ""

plusMinus =+ fun _ -> Negate

equal =+ fun _ -> 
  ExecEqual

pi =+ fun _ ->
  curNum.Text <- string Math.PI
  Validate

input "1"  d1
input "2"  d2
input "3"  d3
input "4"  d4
input "5"  d5
input "6"  d6
input "7"  d7
input "8"  d8
input "9"  d9
input "0"  d0
input "00" d00
input "."  dot

unaryOp Frac      frac 
unaryOp Int       int'
unaryOp Factorial factorial 
unaryOp Reciploc  reciploc
unaryOp Root      root 
unaryOp Exp       exp' 
unaryOp Log       log'
unaryOp Sin       sin' 
unaryOp Sinh      sinh' 
unaryOp Cos       cos' 
unaryOp Cosh      cosh' 
unaryOp Tan       tan' 
unaryOp Tanh      tanh'

binOp Add add
binOp Sub sub
binOp Mul mul
binOp Div div
binOp Mod mod'
binOp Pow pow'

percent =+ fun _ ->
  curNum.Text <- string CalcPercent.Value
  prevNum.Text <- ""
  lastToken <- Token.EqualOp
  Validate

addEqual =+ fun _ -> adder (+)
subEqual =+ fun _ -> adder (-)


// init
Validate

[<EntryPoint; STAThread>]
Application().Run window |> ignore
