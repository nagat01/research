﻿[<AutoOpen>]
module Math

open System

type BinOp = 
  Add | Sub | Mul | Div | Mod | Pow
  member __.exec = 
    match __ with
    | Add -> (+)
    | Sub -> (-)
    | Mul -> (*)
    | Div -> (/)
    | Mod -> (%)
    | Pow -> ( ** )
  override __.ToString() =
    match __ with 
    | Add -> "+"
    | Sub -> "-"
    | Mul -> "×"
    | Div -> "÷"
    | Mod -> "mod"
    | Pow -> "^"

  static member fromString = function
    | "+"   -> Some Add
    | "-"   -> Some Sub
    | "×"   -> Some Mul
    | "÷"   -> Some Div
    | "mod" -> Some Mod
    | "^"   -> Some Pow
    | _     -> None


type UnaryOp = 
  | Frac | Int | Factorial 
  | Reciploc | Root | Exp | Log 
  | Sin | Sinh | Cos | Cosh | Tan | Tanh 
  member __.exec (x:float) = 
    match __ with
    | Frac      -> x - Math.Floor x
    | Int       -> Math.Floor x
    | Factorial -> if abs x > 1000. then Double.NaN else Seq.fold (*) 1. [ yield! [1. .. x]; yield! [x .. -1.]]
    | Reciploc  -> 1. / x 
    | Root      -> sqrt x
    | Exp       -> exp x
    | Log       -> log x
    | Sin       -> sin x
    | Sinh      -> sinh x
    | Cos       -> cos x
    | Cosh      -> cosh x
    | Tan       -> tan x
    | Tanh      -> tanh x

  override __.ToString() =
    match __ with
    | Frac      -> "frac" 
    | Int       -> "int" 
    | Factorial -> "n!"   
    | Reciploc  -> "1/x"   
    | Root      -> "√" 
    | Exp       -> "exp" 
    | Log       -> "log"
    | Sin       -> "sin"
    | Sinh      -> "sinh" 
    | Cos       -> "cos" 
    | Cosh      -> "cosh" 
    | Tan       -> "tan" 
    | Tanh      -> "tanh"

  static member fromString = function
    | "frac" -> Some Frac       
    | "int"  -> Some Int      
    | "n!"   -> Some Factorial 
    | "1/x"  -> Some Reciploc   
    | "√"    -> Some Root     
    | "exp"  -> Some Exp      
    | "log"  -> Some Log      
    | "sin"  -> Some Sin      
    | "sinh" -> Some Sinh      
    | "cos"  -> Some Cos      
    | "cosh" -> Some Cosh      
    | "tan"  -> Some Tan      
    | "tanh" -> Some Tanh
    | _      -> None     

[<RequireQualifiedAccess>]
type Token = BinOp | Num | EqualOp | Adder | None


let numOk x =
  not (Double.IsNaN x) && 
  not (Double.IsInfinity x) 
