﻿[<AutoOpen>]
module Util

open System
open System.Windows
open System.Windows.Controls
open System.Windows.Controls.Primitives
open System.Windows.Media
open System.Windows.Media.Effects


// helper funcs
let (|*>^) x f = f x; x

let inline (<+) set value = 
  (^__: (member Add: _ -> _)(set, value))

let (=+) (button:ButtonBase) f = button.Click.Add f


// util
let parseDouble text =
  try Double.Parse text |> Some with _ -> None


// types
type HA = HorizontalAlignment


